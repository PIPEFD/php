<?php
/*
for ($i = 1; $i <= 15; $i++) {
    echo $i." ";
}
*/

/*
for ($i = 1; ; $i++) {
//----- 2 condicion puede ser omitida-----------    
    if ($i > 10) {
        break;
    }
    echo $i."<br>";
}
*/

/*
//----------------- For que va decrementando-----------

for( $x=5; $x>=1; $x--) {
    echo $x."<br>";
}
*/



$total = 1;

for ($i = 1; $i <= 10; $i++) {
    echo $i."</br>";
    $total += $i;
    /*se realiza la suma del valor anterior y se guarda el valor dentro de "i " en cada vuelta, y el 
    resultado de la operacion es guardado en una nueva variable en estye caso en la variable "$total".*/
}

echo $total;


?>
