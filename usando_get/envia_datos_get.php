<?php


// Convertir una variable en un metododo GET 
/*
$data = array('foo'=>'bar',
              'baz'=>'boom',
              'cow'=>'milk',
              'php'=>'hypertext processor');

echo http_build_query($data) . "<br>";

//se imprime el dato

var_dump (http_build_query($data,'myvar_'));
 
*/
//-----------------------------------------------------------//


//----------------------------------------------------------//
//cuando se usa los caracteres de esta manera se obtiene por seultado la conversion de cadenas de texto o de variables

/*

Output:
--------------------
"array"
  '"' => '&quot;'
  ''' => '&#39;'
  '<' => '&lt;'
  '>' => '&gt;'
  '&' => '&amp;'

'&#039;'

*/

//--------------------------------------------------------------//


//--------------------------------------------------------------//
//Cadena entre comillas dobles convertidada a un caracter especial 
/*
$cadena = '10 es < que 21, y 10 es > que 4, "<estoy entre comillas dobles>"';

echo $cadena;
echo "<br>";
echo htmlspecialchars($cadena);
echo "<br>";
echo htmlspecialchars($cadena, ENT_COMPAT,'ISO-8859-1', true);
*/

//--------------------------------------------------------------//






?>