USE [master]
GO
/****** Object:  Database [Eroski-Txurdinaga]    Script Date: 08/03/2022 19:28:36 ******/
CREATE DATABASE [Eroski-Txurdinaga]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Eroski-Txurdinaga', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MYSQLSERVER\MSSQL\DATA\Eroski-Txurdinaga.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Eroski-Txurdinaga_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MYSQLSERVER\MSSQL\DATA\Eroski-Txurdinaga_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Eroski-Txurdinaga] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Eroski-Txurdinaga].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Eroski-Txurdinaga] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET ARITHABORT OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET RECOVERY FULL 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET  MULTI_USER 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Eroski-Txurdinaga] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Eroski-Txurdinaga] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Eroski-Txurdinaga', N'ON'
GO
ALTER DATABASE [Eroski-Txurdinaga] SET QUERY_STORE = OFF
GO
USE [Eroski-Txurdinaga]
GO
/****** Object:  Table [dbo].[Categoría]    Script Date: 08/03/2022 19:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categoría](
	[ID_Categoria] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Descripción] [varchar](100) NULL,
	[ID_Producto] [varchar](15) NULL,
 CONSTRAINT [PK_Categoría] PRIMARY KEY CLUSTERED 
(
	[ID_Categoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 08/03/2022 19:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[RUT] [varchar](9) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Teléfono1] [int] NULL,
	[Teléfono2] [int] NULL,
	[Dirección] [varchar](150) NULL,
	[IDVenta] [int] NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[RUT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Compra]    Script Date: 08/03/2022 19:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Compra](
	[IDProducto] [varchar](15) NOT NULL,
	[IDVenta] [int] NOT NULL,
	[Cantidad] [int] NULL,
 CONSTRAINT [PK_Compra] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC,
	[IDVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 08/03/2022 19:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[ID_Producto] [varchar](15) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Precio] [float] NULL,
	[Stock] [bit] NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[ID_Producto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 08/03/2022 19:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proveedor](
	[RUT] [varchar](9) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[WEB] [varchar](100) NULL,
	[Dirección] [varchar](150) NULL,
	[Teléfono] [int] NULL,
	[ID_Producto] [varchar](15) NULL,
 CONSTRAINT [PK_Proveedor] PRIMARY KEY CLUSTERED 
(
	[RUT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Venta]    Script Date: 08/03/2022 19:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Venta](
	[ID_Venta] [int] NOT NULL,
	[Fecha] [datetime] NULL,
	[PrecioTotal] [money] NULL,
	[Descuento] [decimal](5, 0) NULL,
 CONSTRAINT [PK_Venta] PRIMARY KEY CLUSTERED 
(
	[ID_Venta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Categoría]  WITH CHECK ADD  CONSTRAINT [FK_Producto_Categoría] FOREIGN KEY([ID_Producto])
REFERENCES [dbo].[Producto] ([ID_Producto])
GO
ALTER TABLE [dbo].[Categoría] CHECK CONSTRAINT [FK_Producto_Categoría]
GO
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Cliente] FOREIGN KEY([IDVenta])
REFERENCES [dbo].[Venta] ([ID_Venta])
GO
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Venta_Cliente]
GO
ALTER TABLE [dbo].[Proveedor]  WITH CHECK ADD  CONSTRAINT [FK_Producto_Proveedor] FOREIGN KEY([ID_Producto])
REFERENCES [dbo].[Producto] ([ID_Producto])
GO
ALTER TABLE [dbo].[Proveedor] CHECK CONSTRAINT [FK_Producto_Proveedor]
GO
USE [master]
GO
ALTER DATABASE [Eroski-Txurdinaga] SET  READ_WRITE 
GO
