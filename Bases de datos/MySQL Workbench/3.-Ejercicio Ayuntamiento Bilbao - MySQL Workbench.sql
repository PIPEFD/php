-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: ayuntamiento_bilbao
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bonobus`
--

DROP TABLE IF EXISTS `bonobus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bonobus` (
  `CodNumerico` int(11) NOT NULL AUTO_INCREMENT,
  `FechaCompra` date DEFAULT NULL,
  `FechaCaducidad` date DEFAULT NULL,
  `DNICiudadano` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`CodNumerico`),
  KEY `bonobus-ciudadano_idx` (`DNICiudadano`),
  CONSTRAINT `bonobus-ciudadano` FOREIGN KEY (`DNICiudadano`) REFERENCES `ciudadano` (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bonobus`
--

LOCK TABLES `bonobus` WRITE;
/*!40000 ALTER TABLE `bonobus` DISABLE KEYS */;
/*!40000 ALTER TABLE `bonobus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudadano`
--

DROP TABLE IF EXISTS `ciudadano`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ciudadano` (
  `DNI` varchar(10) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Apellido` varchar(60) NOT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  PRIMARY KEY (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudadano`
--

LOCK TABLES `ciudadano` WRITE;
/*!40000 ALTER TABLE `ciudadano` DISABLE KEYS */;
/*!40000 ALTER TABLE `ciudadano` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `impuesto_matriculacion`
--

DROP TABLE IF EXISTS `impuesto_matriculacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `impuesto_matriculacion` (
  `Matricula` varchar(8) NOT NULL,
  `Fecha` date NOT NULL,
  `pagado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`Matricula`,`Fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `impuesto_matriculacion`
--

LOCK TABLES `impuesto_matriculacion` WRITE;
/*!40000 ALTER TABLE `impuesto_matriculacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `impuesto_matriculacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linea`
--

DROP TABLE IF EXISTS `linea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `linea` (
  `Número` int(11) NOT NULL,
  `Cabecera` varchar(45) DEFAULT NULL,
  `FinCabecera` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Número`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linea`
--

LOCK TABLES `linea` WRITE;
/*!40000 ALTER TABLE `linea` DISABLE KEYS */;
/*!40000 ALTER TABLE `linea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `padron`
--

DROP TABLE IF EXISTS `padron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `padron` (
  `DNICuidadano` varchar(10) NOT NULL,
  `IDVivienda` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  PRIMARY KEY (`DNICuidadano`,`IDVivienda`,`Fecha`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `padron`
--

LOCK TABLES `padron` WRITE;
/*!40000 ALTER TABLE `padron` DISABLE KEYS */;
/*!40000 ALTER TABLE `padron` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parada`
--

DROP TABLE IF EXISTS `parada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `parada` (
  `Codigo` int(11) NOT NULL,
  `Dirección` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parada`
--

LOCK TABLES `parada` WRITE;
/*!40000 ALTER TABLE `parada` DISABLE KEYS */;
/*!40000 ALTER TABLE `parada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parada-linea`
--

DROP TABLE IF EXISTS `parada-linea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `parada-linea` (
  `CodParada` int(11) NOT NULL,
  `NúmeroLínea` int(11) NOT NULL,
  PRIMARY KEY (`CodParada`,`NúmeroLínea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parada-linea`
--

LOCK TABLES `parada-linea` WRITE;
/*!40000 ALTER TABLE `parada-linea` DISABLE KEYS */;
/*!40000 ALTER TABLE `parada-linea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sorteo`
--

DROP TABLE IF EXISTS `sorteo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sorteo` (
  `NumSorteo` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date DEFAULT NULL,
  `Notario` varchar(45) DEFAULT NULL,
  `Secretario` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`NumSorteo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sorteo`
--

LOCK TABLES `sorteo` WRITE;
/*!40000 ALTER TABLE `sorteo` DISABLE KEYS */;
/*!40000 ALTER TABLE `sorteo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sorteo-ciudadano`
--

DROP TABLE IF EXISTS `sorteo-ciudadano`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sorteo-ciudadano` (
  `IDSorteo` int(11) NOT NULL,
  `DNICiudadano` varchar(10) NOT NULL,
  `afortunado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`IDSorteo`,`DNICiudadano`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sorteo-ciudadano`
--

LOCK TABLES `sorteo-ciudadano` WRITE;
/*!40000 ALTER TABLE `sorteo-ciudadano` DISABLE KEYS */;
/*!40000 ALTER TABLE `sorteo-ciudadano` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trayecto`
--

DROP TABLE IF EXISTS `trayecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trayecto` (
  `CodNumérico` int(11) NOT NULL,
  `CodParadaInicio` int(11) NOT NULL,
  `CodParadaFin` int(11) NOT NULL,
  `Fecha_Hora` datetime(6) NOT NULL,
  PRIMARY KEY (`CodNumérico`,`CodParadaInicio`,`CodParadaFin`,`Fecha_Hora`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trayecto`
--

LOCK TABLES `trayecto` WRITE;
/*!40000 ALTER TABLE `trayecto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trayecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehiculo` (
  `Matricula` varchar(8) NOT NULL,
  `Potencia` float DEFAULT NULL,
  `FechaFabricacion` date DEFAULT NULL,
  `DNICiudadano` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Matricula`),
  KEY `vehiculo-ciudadano_idx` (`DNICiudadano`),
  CONSTRAINT `vehiculo-ciudadano` FOREIGN KEY (`DNICiudadano`) REFERENCES `ciudadano` (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculo`
--

LOCK TABLES `vehiculo` WRITE;
/*!40000 ALTER TABLE `vehiculo` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vivienda`
--

DROP TABLE IF EXISTS `vivienda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vivienda` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Dirección` varchar(100) DEFAULT NULL,
  `ImpuestoPagado` bit(1) DEFAULT NULL COMMENT '0 si no está pagado y 1 si está pagado',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vivienda`
--

LOCK TABLES `vivienda` WRITE;
/*!40000 ALTER TABLE `vivienda` DISABLE KEYS */;
/*!40000 ALTER TABLE `vivienda` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-14 19:39:19
