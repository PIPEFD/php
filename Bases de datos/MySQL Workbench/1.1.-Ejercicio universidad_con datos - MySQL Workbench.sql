-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: universidad
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alumno` (
  `NumMatricula` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) NOT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  `Teléfono` int(11) DEFAULT NULL,
  PRIMARY KEY (`NumMatricula`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES (1,'Felipe','1980-05-06',600000100),(2,'David','1990-04-01',600000200),(3,'Robson','1975-03-04',600000300),(4,'Pamela','1994-01-05',600000400),(5,'Andrés','1995-02-10',600000500),(6,'Iker','1970-08-15',600000600),(7,'Aimar','2000-09-22',600000700),(8,'Paula','2002-12-30',600000800),(9,'Antoni','1991-10-25',600000900),(10,'Sergio','2000-11-19',600001000);
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `asignatura` (
  `CodAsignatura` varchar(20) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `ID_Profesor` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`CodAsignatura`),
  KEY `Profesor-Asignatura_idx` (`ID_Profesor`),
  CONSTRAINT `Profesor-Asignatura` FOREIGN KEY (`ID_Profesor`) REFERENCES `profesor` (`ID_Profesor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignatura`
--

LOCK TABLES `asignatura` WRITE;
/*!40000 ALTER TABLE `asignatura` DISABLE KEYS */;
INSERT INTO `asignatura` VALUES ('UF1841','Elaboración de documentos web mediante lenguajes de marcas','11111111A'),('UF1842','Desarrollo y reutilización de componentes software y multimedia mediante lenguajes de guión','11111111A'),('UF1843','Aplicaciones técnicas de usabilidad y accesibilidad en el entorno cliente','11111111A'),('UF1844','Desarrollo de aplicaciones web en el entorno servidor','22222222B'),('UF1845','Acceso a datos en aplicaciones web del entorno servidor','22222222B'),('UF1846','Desarrollo de aplicaciones web distribuidas','33333333C');
/*!40000 ALTER TABLE `asignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clases`
--

DROP TABLE IF EXISTS `clases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clases` (
  `NumMatricula` int(11) NOT NULL,
  `CodAsignatura` varchar(20) NOT NULL,
  `CursoEscolar` date DEFAULT NULL,
  PRIMARY KEY (`NumMatricula`,`CodAsignatura`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clases`
--

LOCK TABLES `clases` WRITE;
/*!40000 ALTER TABLE `clases` DISABLE KEYS */;
INSERT INTO `clases` VALUES (1,'UF1841','2021-01-01'),(1,'UF1842','2018-01-01'),(1,'UF1843','2019-01-01'),(1,'UF1844','2020-01-01'),(1,'UF1845','2021-01-01'),(1,'UF1846','2021-01-01'),(2,'UF1841','2017-01-01'),(2,'UF1842','2015-01-01'),(2,'UF1843','2020-01-01'),(2,'UF1844','2019-01-01'),(2,'UF1845','2017-01-01'),(2,'UF1846','2018-01-01'),(3,'UF1841','2019-01-01'),(3,'UF1842','2020-01-01'),(3,'UF1843','2015-01-01'),(3,'UF1844','2016-01-01'),(3,'UF1845','2018-01-01'),(3,'UF1846','2020-01-01'),(4,'UF1841','2021-01-01'),(4,'UF1842','2021-01-01'),(4,'UF1843','2018-01-01'),(4,'UF1844','2015-01-01'),(4,'UF1845','2016-01-01'),(4,'UF1846','2017-01-01'),(5,'UF1841','2018-01-01'),(5,'UF1842','2020-01-01'),(5,'UF1843','2016-01-01'),(5,'UF1844','2017-01-01'),(5,'UF1845','2020-01-01'),(5,'UF1846','2015-01-01'),(6,'UF1841','2016-01-01'),(6,'UF1842','2019-01-01'),(6,'UF1843','2018-01-01'),(6,'UF1844','2017-01-01'),(6,'UF1845','2015-01-01'),(6,'UF1846','2016-01-01'),(7,'UF1841','2021-01-01'),(7,'UF1842','2019-01-01'),(7,'UF1843','2018-01-01'),(7,'UF1844','2017-01-01'),(7,'UF1845','2016-01-01'),(7,'UF1846','2015-01-01'),(8,'UF1841','2014-01-01'),(8,'UF1842','2013-01-01'),(8,'UF1843','2013-01-01'),(8,'UF1844','2012-01-01'),(8,'UF1845','2014-01-01'),(8,'UF1846','2015-01-01'),(9,'UF1841','2016-01-01'),(9,'UF1842','2017-01-01'),(9,'UF1843','2018-01-01'),(9,'UF1844','2019-01-01'),(9,'UF1845','2020-01-01'),(9,'UF1846','2021-01-01'),(10,'UF1841','2021-01-01'),(10,'UF1842','2018-01-01'),(10,'UF1843','2017-01-01'),(10,'UF1844','2016-01-01'),(10,'UF1845','2020-01-01'),(10,'UF1846','2019-01-01');
/*!40000 ALTER TABLE `clases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesor`
--

DROP TABLE IF EXISTS `profesor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profesor` (
  `ID_Profesor` varchar(10) NOT NULL,
  `DNI_Profesor` varchar(9) DEFAULT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Especialidad` varchar(35) NOT NULL,
  `Teléfono` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_Profesor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesor`
--

LOCK TABLES `profesor` WRITE;
/*!40000 ALTER TABLE `profesor` DISABLE KEYS */;
INSERT INTO `profesor` VALUES ('11111111A','11111111A','Endika','Informática',600100000),('22222222B','22222222B','Iker','Informática',600200000),('33333333C','33333333C','Joseba','Administración',600300000);
/*!40000 ALTER TABLE `profesor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-14 18:24:49
