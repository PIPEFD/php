<?php
class CuadradoMagico {
    // atributo un cuadrado mágico
    // matriz (array de arrays) de dos dimensiones = 3x3
    private $cuadrado_magico;
    // metodo sobrecargado constructor que acepte como parámetro un cuadrado mágico
    public function __construct($cuadrado) {
        $this->cuadrado_magico=$cuadrado;
        print_r($this->cuadrado_magico);
    }
    // metodo que retorne una cadena con la representación del cuadrado magico en una tabla HTML
    public function crea_cuadrado() {
        $table="<table>";
        for($contFilas=0;$contFilas<3;$contFilas++) {
            $table.="<tr>";
            for($contCols=0;$contCols<3;$contCols++) {            
                $table.="<td>".$this->cuadrado_magico[$contFilas][$contCols]."</td>";
            }            
            $table.="</tr>";
        }
        $table.="</table>";
        return $table;        
    }
    private function suma_fila($numfila) {
        $suma=0;
        for($i=0;$i<3;$i++) {
            $suma+=$this->cuadrado_magico[$numfila][$i];    
        }
        return $suma;
    }
    private function suma_col($numcol) {
        $suma=0;
        for($i=0;$i<3;$i++) {
            $suma+=$this->cuadrado_magico[$i][$numcol];    
        }
        return $suma;
    }
    private function suma_dia($num) {
    }
    // metodo q retorna un booleano indicando si es un cuadrado magico (true) o no (false)
    public function esCuadradoMagico() {
        $esCuadrado=false;
        return $esCuadrado;
    }
}
$cuadrado = array(
    array(5,3,1),
    array(1,2,4),
    array(5,3,6)
 );
$obj = new CuadradoMagico($cuadrado);
echo $obj->crea_cuadrado();
?>