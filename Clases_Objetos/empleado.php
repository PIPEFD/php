<?php
/**
* Empleado
* Confeccionar una clase Empleado, definir como atributos su nombre y sueldo.
* Definir un método inicializarlo para que lleguen como dato el nombre y sueldo.
* Plantear un segundo método que imprima el nombre y un mensaje si debe o no pagar impuestos (si el sueldo supera a 3000 paga impuestos)
 */

class Empleado {
    private $nombre,$sueldo;

    public function establecer_nombre($nombre) {
        $this->nombre = $nombre;
    }

    public function establecer_sueldo($sueldo) {
        $this->sueldo = $sueldo;
    }

    public function establecer_datos($nombre,$sueldo) {
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;
    }

    public function imprime_datos() {
        if($this->sueldo>3000) {
            echo "Hola ".$this->nombre." debes pagar impuestos puto jeta";
        } else {
            echo "Hola ".$this->nombre." eres mileurista, no impuestos"; 
        }
    }
}

$emp = new Empleado();
$emp->establecer_nombre('Iker');
$emp->establecer_sueldo(4000);
$emp->imprime_datos();

$emp = new Empleado();
$emp->establecer_nombre('David');
$emp->establecer_sueldo(1400);
$emp->imprime_datos();

?>