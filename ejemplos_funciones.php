<?php

//----------------------------------------------------//

/*
function myFunc() { echo "Hola mundo<br>"; }

$myFunc = "variable";
myFunc();
*/
//--------------------------------------------------//




/*
function page_header() {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#00001">';
}

page_header();
print "Welcome";
print "</body></html>";
*/



//---------------------------------------------------------------------------//



//---------------------------------------------------------------------------//
//Definiendo funciones antes y después de invocarlas.//


/*
function page_header() {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#B0E0E6	">';
}

page_header();
print "Welcome";
page_footer();

function page_footer() {
	print '<hr>Thanks for visiting.';
	print '</body></html>';
    
}
*/

//--------------------------------------------------------//


//----------------------------------------------------------//

//Esta funcion define un valor como variable y de esta manera es llamdo despues de definir la infucion en el primero momento//

/*
function page_header2($color) {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#' . $color . '">';
}

page_header2('#154674');
print "Welcome!" ;
page_footer2();

function page_footer2() {
	print '<hr>Thanks for visiting.';
	print '</body></html>';
}
*/

//-----------------------------------------------------------//


//---------------------------------------------------------//


/*
function convierte_cadena($str) {
	return strtolower($str);
}

echo convierte_cadena("Hola Mundo!");
*/

//---------------------------------------------------------------------------//



//---------------------------------------------------------------------------//

/*
function page_header4($color, $title) {
	print '<html><head><title>Welcome to ' . $title . '</title></head>';
	print '<body bgcolor="#' . $color . '">';
}
page_header4('66cc66','my homepage');
*/

//---------------------------------------------------------------------------//



//---------------------------------------------------------------------------//

// Retornando un array de una función.
/*
function restaurant_check2($meal, $tax, $tip) {
	$tax_amount = $meal * ($tax / 100);
	$tip_amount = $meal * ($tip / 100);
	$total_notip = $meal + $tax_amount;
	$total_tip = $meal + $tax_amount + $tip_amount;
	return array($total_notip, $total_tip);
}

$totals = restaurant_check2(15.22, 8.25, 15);
if ($totals[0] < 20) {
	print 'The total without tip is less than $20.';
}

if ($totals[1] < 20) {
	print 'The total with tip is less than $20.';
}
*/

//---------------------------------------------------------------------------//



//---------------------------------------------------------------------------//

/*
function payment_method($cash_on_hand, $amount) {
	if ($amount > $cash_on_hand) {
		return 'credit card';
	} else {
		return 'cash';
	}
}

$total = restaurant_check(15.22, 8.25, 15);
$method = payment_method(20, $total);
print 'I will pay with ' . $method;


if (restaurant_check(15.22, 8.25, 15) < 20) {
	print 'Less than $20, I can pay cash.';
} else {
	print 'Too expensive, I need my credit card.';
}
*/
//---------------------------------------------------------------------------//



//---------------------------------------------------------------------------//
// Parámetros por referencia


/*
function parameters_by_reference(&$num) {
	$num = 5;
}

$n = 1;
echo "Valor inicio es: $n<br>";

parameters_by_reference($n);

echo "Ahora es: $n<br>";
*/


//---------------------------------------------------------------------------//



//---------------------------------------------------------------------------//


//Uso de parámetros predeterminados en funciones


function hacer_café($tipo = "capuchino")
{
    return "Hacer una taza de $tipo.\n";
}
echo hacer_café();
echo hacer_café(null);
echo hacer_café("espresso");

//*/
//---------------------------------------------------------------------------//



?>

